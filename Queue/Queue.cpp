#include "Arduino.h"
#include "Queue.h"

Queue::Queue(int floors)
{
    numFloors = floors;
    floorRequested = new bool[numFloors];
    for (int i = 0; i < numFloors; i++)
    {
        floorRequested[i] = false;
    }
}

bool Queue::requestFloor(int floor)
{
    // sanity check that the request is for a valid floor
    if (floor > numFloors || floor <= 0)
    {
        return false;
    }
    else
    {
        floorRequested[floor-1] = true;
        return true;
    }
}

bool Queue::resetFloor(int floor)
{
    // sanity check that the request is for a valid floor
    if (floor > numFloors || floor <= 0)
    {
        return false;
    }
    else
    {
        floorRequested[floor-1] = false;
        return true;
    }
} 

// debugging function to view the array in a nice format in the serial monitor
void Queue::printRequestedFloors()
{
    for (int i = 0; i < numFloors; i++)
    {
        Serial.print("Floor ");
        Serial.print(i + 1);
        Serial.print(": ");
        Serial.println(floorRequested[i]);
    }
}


int Queue::pathFind(int currentFloor) {
    
    int here = currentFloor;
    int i = currentFloor;

   while(i > 0 && i <= numFloors) { //while index still exists basically

    if(floorRequested[i] == true) {
        move(i - here);
        break;
    }

    if(UP == true)
    i++;                //if elevator state is up
    else
    i--;                //if elevator state is down
 
    if(i > numFloors) 
    UP = false;

    if(i < 0)
    UP = true;


   }
   
   
   
   
    /*for(;;)  {

        if(UP) {
        i++; } else {
            i--;
        }
        
        if(floorRequested[i] == true) {
            move(i- currentFloor); //motions write move?
            resetFloor(i);
            break;
        }

        if(i <= 0)
        UP = true;

        if(i >= numFloors)
        UP = false;

    } */


}

int Queue::moveUpOne(int currentFloor) {
    if(currentFloor < numFloors) {
    return currentFloor 1;
    }
    return 0;
}

int Queue::moveDownOne(int currentFloor) {
    if(currentFloor>0) {
        return -1;
    }
    return 0;
}
//this is a change just to test git add/commit/push