#ifndef QUEUE
#define QUEUE

#include "Arduino.h"

class Queue
{
    
    //attribures
        int numFloors;              //number of floors
        bool floorRequested[5];     //I dont like pointers but maybe change to pointer
        bool UP;                    //state
       
        //constructor
        Queue(int floors);
        
        //methods
        bool requestFloor(int floor);
        bool resetFloor(int floor);
        void printRequestedFloors();
        int pathFind(int currentFloor);
        
        //just for testing if pathfind doesnt work
        int moveUpOne(int currentFloor);    
        int moveDownOne(int currentFloor);  
    
    
    //sams original code below
    
    /*public:
        Queue(int floors);
        int numFloors;
        bool* floorRequested;
        bool requestFloor(int floor);
        bool resetFloor(int floor);
        void printRequestedFloors(); */
};

#endif
