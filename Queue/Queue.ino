/*    Enable debug mode    */
#define ENABLE_DEBUG

/*    Constants    */
#define NUMFLOORS 5
#define DOORTOGGLE 6
#define EMERGENCY 7

/*    Library Includes   */
#include "Queue.h"

Queue q(NUMFLOORS);

void setup()
{
  Serial.begin(9600);
  #ifdef ENABLE_DEBUG
  Serial.println("Serial is ready");
  Serial.println("Printing the initial state of elevator memory");
  q.printRequestedFloors();

  Serial.println("Testing setting each floor");
  for (int i = 1; i <= NUMFLOORS; i++)
  {
    q.requestFloor(i);
    q.printRequestedFloors();
    Serial.println();
  }
 
  Serial.println("Testing resetting each floor");

  for (int i = 1; i <= NUMFLOORS; i++)
  {
    q.resetFloor(i);
    q.printRequestedFloors();
    Serial.println();
  }
  #endif
}

void loop()
{
  if (Serial.available() > 0)
  {
    String s = Serial.readString();
    s.trim();
    int i = s.toInt();
    if (i == 0)
    {
      if (s.equals("print"))
      {
        q.printRequestedFloors();
      }
      else
      {
        Serial.println("InvalidRequest");
      }
    }
    else
    {
      #ifdef ENABLE_DEBUG
      Serial.print("Received input: ");
      Serial.println(i);
      #endif

      Serial.println(10);

      if (i == EMERGENCY)
      {
        #ifdef ENABLE_DEBUG
        Serial.println("Proceeding with emergency protocol");
        #endif
        // PROCEED TO EMERGENCY PROTOCOL
      }
      else if (i == DOORTOGGLE)
      {
        #ifdef ENABLE_DEBUG
        Serial.println("Proceeding with door toggling");
        #endif
        // PROCEED TO TOGGLE DOORS IF IN A VALID STATE
      }
      else if (i > 0 && i <= NUMFLOORS)
      {
        #ifdef ENABLE_DEBUG
        Serial.println("Proceeding with floor request");
        #endif
        q.requestFloor(i);
      }
      else
      {
        #ifdef ENABLE_DEBUG
        Serial.println("InvalidRequest");
        #endif
      }
    }
  }
}
