#include "Arduino.h"
#include "Queue.h"

Queue::Queue(int floors)
{
    numFloors = floors;
    floorRequested = new bool[numFloors];
    for (int i = 0; i < numFloors; i++)
    {
        floorRequested[i] = false;
    }
    currentFloor = 0;
    up = false;
}

bool Queue::requestFloor(int floor)
{
    // sanity check that the request is for a valid floor
    if (floor > numFloors || floor <= 0)
    {
        return false;
    }
    else
    {
        floorRequested[floor-1] = true;
        return true;
    }
}

bool Queue::resetFloor(int floor)
{
    // sanity check that the request is for a valid floor
    if (floor >= numFloors || floor < 0)
    {
        return false;
    }
    else
    {
        floorRequested[floor] = false;
        return true;
    }
} 



int Queue::getMove(){
  int i = currentFloor;
  bool switched = false;
  resetFloor(currentFloor);
  while(i>=0 && i<numFloors){
    if(floorRequested[i]){
      Serial.println("hi");
      return(i-currentFloor);
    }
    if(up){
      i++;
    }else{
      i--;
    }
    if(!switched && (i < 0 || i >= numFloors)){
      up = !up;
      i = currentFloor;
      switched = !switched;
    }
  }
  // if no floor is found, defaults to current direction as down
  up = false;
  // Serial.println("No floor requested");
  return 0;
}

void Queue::printFloors()
{
  for (int i = 0; i < numFloors; i++)
  {
    Serial.print(i);
    Serial.print(":");
    Serial.print(floorRequested[i]);
    Serial.print(",");
  }
  Serial.println();
}
